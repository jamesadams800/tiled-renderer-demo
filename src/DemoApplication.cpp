#include "DemoApplication.h"

#include <string>
#include <sstream>
#include <random>

#include "SceneLoader.h"
#include "Scene.h"
#include "MoveableCamera.h"
#include "TextUiElement.h"

namespace demo {

using namespace renderer;

DemoApplication::DemoApplication()
  : GLApplication("Demo Application"),
    point_lights_{} {
}

void DemoApplication::InitScene(Scene& scene) {
  SceneLoader::LoadScene("assets\\models\\sponza.obj", scene, SceneLoader::ASSIMP);

  // Add frame counter text element.
  frame_time_text_ = TextUiElement::Create("", glm::vec2(0.02f, 0.97f));
  frame_time_text_->SetFontSize(10);
  scene.GetUi().AddElement(frame_time_text_);
  scene.GetUi().AddElement(TextUiElement::Create("Toggle Full Screen = F10", glm::vec2(0.02f, 0.94), glm::vec3(0.67f, 0.02f, 0.0f)));
  scene.GetUi().AddElement(TextUiElement::Create("Free Mouse = Space", glm::vec2(0.02f, 0.92f), glm::vec3(0.67f, 0.02f, 0.0f)));

  // Create the controllable camera, configure it and add it to scene.
  std::shared_ptr<MoveableCamera> camera = MoveableCamera::Create();
  this->AddKeyListener(camera.get());
  this->AddMouseListener(camera.get());
  camera->SetVerticalFov(50.0f);
  camera->SetNearPlane(10.0f);
  camera->SetFarPlane(3000.0f);
  camera->LookAt(0.0f, 0.0f, -1.0f);
  camera->SetMoveSpeed(10.0f);
  scene.SetCamera(camera);

  // Create 4096 randomly generated point lights, along with their paths.
  std::default_random_engine generator;
  std::uniform_real_distribution<float> distribution(0.0f, 1.0f);
  for (uint32_t i = 0; i < 4096; i++) {
    std::shared_ptr<PointLight> point_light = PointLight::Create(glm::vec3(distribution(generator),
                                                                           distribution(generator),
                                                                           distribution(generator)), distribution(generator) * 5.0f);
    float radius = 250.0f + 1200.0f * distribution(generator);
    float radius2 = 250.0f + 650.0f * distribution(generator);
    float height = 1000.0f * distribution(generator);
    float offset = 2.0f * 3.411 * distribution(generator);
    float x = radius * cosf(offset);
    float z = radius2 * sinf(offset);
    light_offsets_.push_back(glm::vec4(radius, radius2, offset, height));
    point_light->SetTranslation(x, height, z);
    point_light->SetQuadraticAttenuation(0.05f + distribution(generator) * 0.1f);
    point_light->SetLinearAttenuation(distribution(generator) * 0.f);
    point_lights_.push_back(point_light);
    scene.AddLight(point_light);
  }

  glm::vec3 light_direction = glm::normalize(glm::vec3(-.4f, -1.0f, -0.14f));
  auto directional_light = DirectionalLight::Create(light_direction, glm::vec3(0.8f, 0.8f, 0.756f), 2.0f);
  directional_light->AddShadowMap(*camera);
  scene.AddLight(directional_light);
}

void DemoApplication::Update(Scene& scene, float dt) {
  static float local_timer = 0;
  static uint32_t frames = 0;
  float x = std::sinf(light_timer_) * std::sinf(light_timer_);
  float y = 1.0f;
  float z = std::sinf(light_timer_) * std::cosf(light_timer_);

  // Move lights around scene.
  light_timer_ += dt * 0.0001f;
  for (uint32_t i = 0; i < point_lights_.size(); i++) {
    const glm::vec4& offsets = light_offsets_[i];
    PointLight& point_lights = *point_lights_[i];

    float x = offsets.x * cosf(offsets.z + light_timer_);
    float y = offsets.w;
    float z = offsets.y * sinf(offsets.z + light_timer_);

    point_lights.SetTranslation(glm::vec3(x, y, z));
  }

  // Update frame counter.
  frames++;
  local_timer += dt;
  if (local_timer > 200.0f) {
    float average_ms = local_timer / float(frames);
    local_timer = 0.0f;
    frames = 0;
    std::ostringstream ss;
    ss.precision(4);
    ss << "FPS : " << 1000.0f / average_ms << " Frame time : " << std::fixed << average_ms << " ms";
    frame_time_text_->UpdateText(ss.str());
  }
}

} // namespace demo