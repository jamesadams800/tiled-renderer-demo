#include "DemoApplication.h"

#include <iostream>

int main() {
  demo::DemoApplication().Run();

  return 0;
}