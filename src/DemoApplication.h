#ifndef DEMO_APPLICATION_H_
#define DEMO_APPLICATION_H_

#include <memory>
#include <vector>

#include <GLApplication.h>
#include <PointLight.h>
#include <TextUiElement.h>

namespace demo {

class DemoApplication : public renderer::GLApplication {
public:
  DemoApplication();

  void InitScene(renderer::Scene& scene) override;
  void Update(renderer::Scene& scene, float dt) override;

private:
  float light_timer_;
  std::vector<glm::vec4> light_offsets_;
  std::vector<std::shared_ptr<renderer::PointLight>> point_lights_;
  std::shared_ptr<renderer::TextUiElement> frame_time_text_;
};

} // namespace demo

#endif // DEMO_APPLICATION_H_