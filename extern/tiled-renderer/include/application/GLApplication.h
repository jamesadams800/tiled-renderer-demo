#ifndef OGL_GLAPPLICATION_H_
#define OGL_GLAPPLICATION_H_

#include <vector>
#include <memory>

#include "Common.h"
#include "IKeyListener.h"
#include "IMouseListener.h"

namespace renderer {

class GLWindow;
class Scene;

class GLApplication {
public:
  class Impl;

  RENDER_API explicit GLApplication(const std::string& name);
  RENDER_API virtual ~GLApplication();

  // Call this to run the application...
  RENDER_API void Run();
protected:
  // Use these two functions to register objects to recieve device input. For now any objects
  // that are added for device input MUST have a lifetime that is as long as the containing
  // GLApplication object.
  RENDER_API void AddKeyListener(IKeyListener* listener);
  RENDER_API void AddMouseListener(IMouseListener* listener);
private:

  // Scene Initialization for rendering.  IMPORTANT! do not attempt to initialise any
  // Scene objects before this function.  It is fine to allocate pointers up front as
  // class members but DO NOT create objects and bind them.  The creation of all scene
  // objects must be done in this InitScene override by the deriving class.
  RENDER_API virtual void InitScene(Scene& scene) = 0;

  // Callback called once a frame.  Any scene modification to be done in here
  // dt is time delta in ms.
  RENDER_API virtual void Update(Scene& scene, float dt) = 0;

  std::unique_ptr<Impl> impl_;
};

} // namespace renderer 

#endif // OGL_GLAPPLICATION_H_