#ifndef OGL_IMOUSELISTENER_H_
#define OGL_IMOUSELISTENER_H_

#include <stdint.h>

namespace renderer {

class RENDER_API IMouseListener {
public:

  enum MouseEvent {
    MOUSE_BUTTON_PRESSED,
    MOUSE_BUTTON_HELD,
    MOUSE_BUTTON_RELEASED
  };

  enum MouseButton {
    MOUSE_1,
    MOUSE_2,
    MOUSE_3,
    MOUSE_SCROLL_UP,
    MOUSE_SCROLL_DOWN
  };

  virtual ~IMouseListener() = default;

  virtual void MouseMoved(double dx, double dy) = 0;
  virtual void MousePressed(double dx, double dy, MouseButton, MouseEvent) = 0;
};

}// namespace renderer
#endif // OGL_IMOUSELISTENER_H_
