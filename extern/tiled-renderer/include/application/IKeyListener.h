#ifndef OGL_IKEYLISTENER_H_
#define OGL_IKEYLISTENER_H_

#include "Common.h"

namespace renderer {

class RENDER_API IKeyListener {
public:
  enum KeyEvent {
    KEY_PRESSED,
    KEY_HELD,
    KEY_RELEASED
  };

  enum Key {
    KEY_W,
    KEY_S,
    KEY_A,
    KEY_D,
    KEY_F,
    KEY_I,
    KEY_J,
    KEY_K,
    KEY_L,
    KEY_U,
    KEY_O,
    KEY_R,
    KEY_T,
    KEY_P,
    KEY_SPACE,
    KEY_ENTER,
    KEY_PLUS,
    KEY_MINUS
  };

  virtual ~IKeyListener() {
  }

  virtual void KeyEventPerformed(Key key, KeyEvent event ) = 0;
};

}// namespace renderer
#endif // OGL_IKEYLISTENER_H_