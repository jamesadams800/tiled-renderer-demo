#ifndef OGL_OBJECT_H_
#define OGL_OBJECT_H_

#include <memory>

#include "Common.h"

namespace renderer {

class Object {
public:
  template <class T,class ...Args>
  static std::shared_ptr<T> CreateSharedObject(Args&&... args) {
    return std::shared_ptr<T>(new T(std::forward<Args>(args)...), BaseDeleter());
  }

  struct RENDER_API BaseDeleter {
    void operator()(Object* base);
  };

  virtual ~Object() {
  }

protected:
  Object() {}
};

} // namespace renderer

#endif
