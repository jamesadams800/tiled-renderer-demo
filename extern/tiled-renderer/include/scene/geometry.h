#ifndef OGL_GEOMETRY_H_
#define OGL_GEOMETRY_H_

#include <vector>
#include <stdint.h>

#include "VertexBuffer.h"
#include "IndexBuffer.h"
#include "AABB.h"
#include "Common.h"

namespace renderer {

class Geometry {
public:
  enum Primitive {
    POINTS,
    LINES,
    TRIANGLES
  };

  Geometry() = delete;
  explicit Geometry(Primitive primitive_type = TRIANGLES);
  explicit Geometry(std::unique_ptr<VertexBuffer>,
                    Primitive primitive_type = TRIANGLES);
  explicit Geometry(std::unique_ptr<VertexBuffer>,
                    std::unique_ptr<IndexBuffer>,
                    Primitive primitive_type = TRIANGLES);

  ~Geometry();

  RENDER_API void SetVertexBuffer(std::unique_ptr<VertexBuffer> buffer);
  RENDER_API void SetIndexBuffer(std::unique_ptr<IndexBuffer> buffer);
  RENDER_API void SetPrimitiveType(Primitive primitive_type);

  Primitive GetPrimitiveType() const {
    return primitive_type_;
  }

  const VertexBuffer& GetVertexBuffer() const {
    return *vertex_buffer_;
  }

  VertexBuffer& GetVertexBuffer() {
    return *vertex_buffer_;
  }

  bool IsIndexed() const {
    return is_indexed_;
  }

  const IndexBuffer& GetIndexBuffer() const {
    return *index_buffer_;
  }

  IndexBuffer& GetIndexBuffer() {
    return *index_buffer_;
  }

  void Bind() const;

  const AABB& GetAABB() const {
    return aabb_;
  }

private:
  void CreateGpuObject();
  void UpdateGpuObject();
  void DeleteGpuObject();

  uint32_t vao_handle_;
  Primitive primitive_type_;
  std::unique_ptr<VertexBuffer> vertex_buffer_;
  bool is_indexed_;
  std::unique_ptr<IndexBuffer> index_buffer_;
  AABB aabb_;
};

} // namespace renderer

#endif // OGL_GEOMETRY_H_