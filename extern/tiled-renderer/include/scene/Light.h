#ifndef OGL_LIGHT_H_
#define OGL_LIGHT_H_

#include <memory>

#include "Entity.h"

namespace renderer {

class Light : public Entity {
public:
  explicit Light(const glm::vec3& color, float intensity = 1.0f);

  bool PendingWrite() const {
    return dirty_;
  }

  RENDER_API void SetTransform(const glm::mat4& transform) override;
  RENDER_API void SetTranslation(const glm::vec3& vec) override;
  RENDER_API virtual void SetTranslation(float x, float y, float z) override;
  RENDER_API virtual void SetRotation(const glm::vec3& axis, float rad) override;
  RENDER_API virtual void SetIntensity(float intensity);
  RENDER_API float GetIntensity() const;
  RENDER_API virtual void SetColor(const glm::vec3& color);

  glm::vec3 GetColor() const {
    return color_;
  }

  static float RgbToLuminosity(const glm::vec3 light_rgb) {
    glm::vec3 rgb_to_luminance(0.212f, 0.715f, 0.072f);
    return glm::dot(light_rgb, rgb_to_luminance);
  }
protected:
  mutable bool dirty_;
private:
  float intensity_;
  glm::vec3 color_;
};

} // namespace renderer

#endif // OGL_LIGHT_H_