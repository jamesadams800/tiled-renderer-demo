#ifndef OGL_ORTHOGRAPHICCAMERA_H_
#define OGL_ORTHOGRAPHICCAMERA_H_

#include "Camera.h"

namespace renderer {

class OrthographicCamera : public Camera {
public:
  RENDER_API static std::shared_ptr<OrthographicCamera> Create();
  OrthographicCamera();

  RENDER_API void SetLeft(float left);
  RENDER_API void SetRight(float right);
  RENDER_API void SetTop(float top);
  RENDER_API void SetBottom(float bottom);

  RENDER_API void CalculateProjectionMatrix() override final;
  RENDER_API std::array<glm::vec4, 8> GetSplitFrustrum(float, float) const override final;

private:
  float left_;
  float right_;
  float top_;
  float bottom_;

  virtual std::array<glm::vec4, 8> GetWorldSpaceSplitFrustrum(float, float) const override;
  virtual void CalculateFrustrum() override;
};

} // namespace renderer

#endif // OGL_ORTHOGRAPHICCAMERA_H_