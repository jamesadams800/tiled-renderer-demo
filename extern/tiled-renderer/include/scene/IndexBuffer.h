#ifndef OGL_INDEXBUFFER_H_
#define OGL_INDEXBUFFER_H_

#include <vector>

#include "Object.h"

namespace renderer {

class IndexBuffer : public Object {
public:

  RENDER_API static std::shared_ptr<IndexBuffer> Create();
  
  explicit IndexBuffer();
  explicit IndexBuffer(const std::vector<uint32_t>& data);
  virtual ~IndexBuffer();

  RENDER_API IndexBuffer(const IndexBuffer&);
  RENDER_API IndexBuffer& operator=(IndexBuffer);
  RENDER_API IndexBuffer(const IndexBuffer&&);
  RENDER_API IndexBuffer& operator=(const IndexBuffer&&);
  RENDER_API friend void swap(IndexBuffer& first, IndexBuffer& second);
  RENDER_API virtual std::shared_ptr<IndexBuffer> Clone();

  RENDER_API void AddIndexData(const std::vector<uint32_t>& data);
  RENDER_API bool IsIndexPresent(uint32_t) const;
  RENDER_API void AddIndex(uint32_t);
  RENDER_API void RemoveIndex(uint32_t);
  RENDER_API void ClearIndexData();

  size_t Size() const {
    return index_array_.size();
  }

  uint32_t operator[](uint32_t i) const {
    return index_array_[i];
  }
 
  const std::vector<uint32_t>& GetIndexArray() const {
    return index_array_;
  }

  void Bind() const;
private:
  void CreateGpuObject();
  void UpdateGpuObject();
  void DeleteGpuObject();

  uint32_t index_buffer_handle_;
  std::vector<uint32_t> index_array_;
};

} // namespace renderer

#endif // OGL_INDEXBUFFER_H_