#ifndef OGL_POINTLIGHT_H_
#define OGL_POINTLIGHT_H_

#include "Light.h"

namespace renderer {

class PointLight : public Light {
public:
  struct ShaderData;

  RENDER_API static std::shared_ptr<PointLight> Create(const glm::vec3& color,
                                                          float intensity = 1.0f);
  explicit PointLight(const glm::vec3& color, float intensity = 1.0f);

  RENDER_API void SetLinearAttenuation(float linear);
  RENDER_API void SetQuadraticAttenuation(float quadratic);
  RENDER_API float GetLinearAttenuation() const;
  RENDER_API float GetQuadraticAttenuation() const;
  RENDER_API float GetRadius() const;
  RENDER_API void SetIntensity(float intensity) override;
  RENDER_API void SetColor(const glm::vec3& color) override;

  void WriteShaderData(ShaderData&) const;

private:
  static constexpr float MINIMUM_LIGHT_INTENSITY = 0.001f;
  void CalculateRadius();

  float light_radius_;
  float linear_attenuation_;
  float quadratic_attenuation_;
};

} // namespace renderer 

#endif // OGL_POINTLIGHT_H_
