#ifndef OGL_TEXTUIELEMENT_H_
#define OGL_TEXTUIELEMENT_H_

#pragma warning(push, 0)  
#include <glm/glm.hpp>
#include <glm/ext.hpp>
#pragma warning(pop)

#include "UserInterface.h"
#include "Geometry.h"

namespace renderer {

class GlyphLibrary;
template <class T> class Texture;

class TextUiElement : public UiElement {
public:
  RENDER_API static std::shared_ptr<TextUiElement> Create(const std::string& text,
                                                             const glm::vec2& position,
                                                             const glm::vec3& color = 
                                                             glm::vec3(1.0f,1.0f,1.0f));

  TextUiElement();
  explicit TextUiElement(const std::string& text, const glm::vec2& position);
  explicit TextUiElement(const std::string& text, const glm::vec2& position, const glm::vec3& color);

  RENDER_API void UpdateText(const std::string& text);

  // Font size in pt.
  RENDER_API void SetFontSize(uint32_t size);
  RENDER_API void SetColor(const glm::vec3& color);
  RENDER_API uint32_t GetFontSize() const;
  RENDER_API glm::vec2 GetPosition() const;
  RENDER_API glm::vec2 GetTextDims() const;
  RENDER_API glm::vec3 GetColor() const;

  void Render(UiRenderPass& pass) override;
  void Bind() const;
  glm::mat4 GetScale() const {
    return text_scale_;
  }

private:
  void BuildTexture();
  void Update() override;

  std::string text_;
  // In screen space.
  glm::vec2 position_;
  glm::vec3 color_;
  float length_ratio_;
  uint32_t font_size_;
  glm::mat4 text_scale_;
  Geometry text_geometry_;
  std::unique_ptr<Texture<uint8_t>> string_texture_;
  static GlyphLibrary glyphs_;
  static std::vector<float> kPositions_;
  static std::vector<float> kTexCoords_;
};

}

#endif // OGL_TEXTUIELEMENT_H_
