#ifndef OGL_MESH_H_
#define OGL_MESH_H_

#include <memory>

#include "AABB.h"
#include "Renderable.h"
#include "Geometry.h"

namespace renderer {

class Mesh : public Renderable {
public:
  RENDER_API static std::shared_ptr<Mesh> Create();
  Mesh();
  explicit Mesh(std::unique_ptr<VertexBuffer>);
  explicit Mesh(std::unique_ptr<VertexBuffer>, std::unique_ptr<IndexBuffer>);

  RENDER_API void SetName(const std::string& name);
  RENDER_API std::string GetName() const;
  RENDER_API const Geometry& GetGeometry() const;
  RENDER_API Geometry& GetGeometry();
  RENDER_API const AABB& GetAABB() const;

  void Accept(RenderVisitor&) override;

private:
  static uint32_t mesh_id_counter_;
  std::string name_;
  Geometry geometry_;
};

} // namespace renderer

#endif // OGL_MESH_H_