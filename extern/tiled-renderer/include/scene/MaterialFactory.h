#ifndef OGL_MATERIALFACTORY_H_
#define OGL_MATERIALFACTORY_H_

#include "Material.h"

namespace renderer {

class MaterialFactory {
public:
  RENDER_API static std::shared_ptr<Material> BuildXorMaterial(const glm::vec3& color,
                                                                  float shininess = 5.0f);
  RENDER_API static std::shared_ptr<Material> BuildGridMaterial(const glm::vec3& color,
                                                                   float shininess = 5.0f);
};

} // namespace renderer

#endif // OGL_MATERIALFACTORY_H_