#ifndef OGL_RENDERABLE_H_
#define OGL_RENDERABLE_H_

#include "Entity.h"
#include "Material.h"
#include "AABB.h"

namespace renderer {

class Renderer;
class RenderVisitor;

class Renderable : public Entity {
public:
  Renderable();

  RENDER_API void SetScale(float x, float y, float z);
  RENDER_API void SetScale(const glm::vec3& scale);
  RENDER_API bool IsVisible() const;
  RENDER_API void SetVisible(bool visible);
  RENDER_API void SetMaterial(std::shared_ptr<Material> material);
  RENDER_API virtual const AABB& GetAABB() const = 0;

  const Material* GetMaterial() const;
  virtual void Accept(RenderVisitor& renderer) = 0;
private:
  bool visible_;
  std::shared_ptr<Material> material_;
};

} // namespace renderer 

#endif // OGL_RENDERABLE_H_