#ifndef OGL_AABB_H_
#define OGL_AABB_H_

#include <utility>
#include <memory>

#pragma warning(push, 0)  
#include <glm/glm.hpp>
#include <glm/ext.hpp>
#pragma warning(pop)

#include "Object.h"

namespace renderer {

class VertexBuffer;

class AABB : public Object {
public:
  static_assert(std::numeric_limits<float>::is_iec559, "IEEE 754 required");

  RENDER_API static std::shared_ptr<AABB> Create();

  AABB();
  explicit AABB(const glm::vec3& min, const glm::vec3& max);
  ~AABB();

  AABB(const AABB&);
  AABB& operator=(const AABB&);

  RENDER_API void FitBox(const VertexBuffer& );
  RENDER_API void Set(const glm::vec3& min, const glm::vec3& max);
  RENDER_API void Merge(const AABB& other);
  RENDER_API const glm::vec3& GetMin() const;
  RENDER_API const glm::vec3& GetMax() const;
  RENDER_API void SetTransform(const glm::mat4& transform);
  RENDER_API friend AABB operator*(const AABB& aabb, const glm::mat4& transform);
  RENDER_API std::pair<glm::vec3, glm::vec3> GetBounds() const;
  RENDER_API friend std::ostream& operator<<(std::ostream&, const AABB&);
private:
  glm::vec3 min_;
  glm::vec3 max_;
};

} // namespace renderer

#endif // OGL_AABB_H_