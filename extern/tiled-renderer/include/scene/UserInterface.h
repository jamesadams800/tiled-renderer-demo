#ifndef OGL_USERINTERFACE_H_
#define OGL_USERINTERFACE_H_

#include <memory>
#include <map>

#include "Object.h"

namespace renderer {

class UiRenderPass;

using ElementID = uint32_t;

class UiElement : public Object {
public:
  virtual void Render(UiRenderPass& renderer) = 0;
  virtual void Update() = 0;

  RENDER_API ElementID GetId() const;

private:
  friend class UserInterface;
  void SetId(ElementID id) {
    element_id_ = id;
  }

  ElementID element_id_;
};

class UserInterface {
public:
  UserInterface();
  UserInterface(const UserInterface&) = delete;
  UserInterface& operator=(const UserInterface&) = delete;

  // Once a UI Element has been added to the UI the UI takes ownership of that element.
  RENDER_API ElementID AddElement(std::shared_ptr<UiElement> element);
  RENDER_API void DeleteElement(ElementID id);

  const std::map<ElementID, std::shared_ptr<UiElement>>& GetElements() const {
    return ui_elements_;
  }

private:
  ElementID interface_id_counter_;
  std::map<ElementID, std::shared_ptr<UiElement>> ui_elements_;
};

}

#endif // OGL_USERINTERFACE_H_