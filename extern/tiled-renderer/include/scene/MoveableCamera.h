#ifndef OGL_MOVEABLECAMERA_H_
#define OGL_MOVEABLECAMERA_H_

#include "PerspectiveCamera.h"
#include "IKeyListener.h"
#include "IMouseListener.h"

namespace renderer {

class MoveableCamera : public PerspectiveCamera, public IKeyListener, public IMouseListener {
public:
  RENDER_API static std::shared_ptr<MoveableCamera> Create();
  MoveableCamera();

  RENDER_API bool IsLocked() const;
  RENDER_API float GetMoveSpeed() const;
  RENDER_API void SetMoveSpeed(float camera_speed);

private:
  void KeyEventPerformed(Key key, KeyEvent event) override;
  void MouseMoved(double dx, double dy) override;
  void MousePressed(double dx, double dy, MouseButton, MouseEvent) override;

  float camera_move_speed_;
  float cursor_dx_;
  float cursor_dy_;
  bool move_camera_;
};

} // namespace renderer

#endif // OGL_MOVEABLECAMERA_H_