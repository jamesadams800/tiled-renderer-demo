#ifndef OGL_SCENE_H_
#define OGL_SCENE_H_

#include <vector>
#include <memory>
#include <iostream>
#include <unordered_map>

#include "Camera.h"
#include "Renderable.h"
#include "PointLight.h"
#include "DirectionalLight.h"
#include "Material.h"
#include "UserInterface.h"

namespace renderer {

class Scene {
public:
  class Impl;
  Scene(Impl&);

  RENDER_API void SetCamera(std::shared_ptr<Camera> camera);
  RENDER_API void AddRenderable(std::shared_ptr<Renderable> renderable);
  RENDER_API void AddLight(std::shared_ptr<DirectionalLight> light);
  RENDER_API void AddLight(std::shared_ptr<PointLight> light);

  RENDER_API void AddTexture(const std::string& string, std::shared_ptr<Texture<uint8_t>> texture);
  RENDER_API void AddMaterial(uint64_t material_index, std::shared_ptr<Material> material);

  RENDER_API std::shared_ptr<Material> GetMaterial(uint64_t id);
  RENDER_API std::shared_ptr<Texture<uint8_t>> GetTexture(const std::string& name);

  RENDER_API uint64_t GetNextMaterialId();

  RENDER_API UserInterface& GetUi();
  RENDER_API const UserInterface& GetUi() const;

private:
  Impl& impl_;
};

} // namespace renderer

#endif // OGL_SCENE_H_