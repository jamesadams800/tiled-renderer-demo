#ifndef OGL_PERSPECTIVECAMERA_H_
#define OGL_PERSPECTIVECAMERA_H_

#include "Camera.h"

namespace renderer {

class PerspectiveCamera : public Camera {
public:
  RENDER_API static std::shared_ptr<PerspectiveCamera> Create();
  PerspectiveCamera();

  RENDER_API void SetVerticalFov(float fov);
  RENDER_API float GetVerticalFov() const;
  RENDER_API void CalculateProjectionMatrix() override final;
  RENDER_API std::array<glm::vec4, 8> GetSplitFrustrum(float, float) const override final;
  RENDER_API std::array<glm::vec4, 8> GetWorldSpaceSplitFrustrum(float, float) const override final;

private:
  void CalculateFrustrum() override final;

  float fov_;
  float vertical_fov_tan_;
  float horizontal_fov_tan_;
};

} // namespace renderer

#endif // OGL_PERSPECTIVECAMERA_H_