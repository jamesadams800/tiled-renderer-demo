#ifndef OGL_DIRECTIONALLIGHT_H_
#define OGL_DIRECTIONALLIGHT_H_

#include "Light.h"
#include "AABB.h"
#include "Camera.h"

namespace renderer {

class Scene;
class ShadowPass;
class CascadedShadowMap;

class DirectionalLight : public Light {
 public:
  struct ShaderData;

  RENDER_API static std::shared_ptr<DirectionalLight> Create(const glm::vec3& direction,
                                                                const glm::vec3& color,
                                                                float intensity = 1.0f);

  explicit DirectionalLight(const glm::vec3& direction,
                            const glm::vec3& color,
                            float intensity = 1.0f);
  explicit DirectionalLight(const glm::vec3& color, float intensity = 1.0f);

  RENDER_API void AddShadowMap(const Camera& camera);
  RENDER_API void SetDirection(const glm::vec3& direction);
  RENDER_API glm::vec3 GetDirection() const;

  void WriteShaderData(ShaderData&) const;

  CascadedShadowMap* GetShadowMap() {
    return shadow_map_.get();
  }

 private:
  std::unique_ptr<CascadedShadowMap> shadow_map_;
  glm::vec3 direction_;
};

}  // namespace renderer

#endif  // OGL_DIRECTIONALLIGHT_H_
