#version 450
// IN
layout( location = 0 ) in vec2 tex_coord;

// OUT
layout(location = 0) out vec4 out_color;

uniform sampler2D text;
uniform vec3 color;

void main() {
	float tex = texture2D( text, tex_coord ).r;
	out_color = tex * vec4(color, 1.0f);
}