#version 450

layout( location = 0 ) in flat uint id;

struct PointLight {
  mat4 model_matrix;
  vec4 color;
  vec4 position;
  float radius;
  float linear_attenuation;
  float quadratic_attenuation;
};

layout(std140) buffer PointLights {
  PointLight lights[];
};

layout( location = 0 ) out vec4 out_color;

void main() {
  // Bit of a hack but make sources appear bright.
  out_color = 1.5f * lights[id].color;
}