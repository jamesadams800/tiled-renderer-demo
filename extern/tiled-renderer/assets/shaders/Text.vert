#version 450
// IN 
layout( location = 0 ) in vec3 position;
layout( location = 1 ) in vec2 raw_tex_coord;
// OUT
layout( location = 0 ) out vec2 tex_coord;

uniform mat4 projection;
uniform vec2 offset;

void main() {
  tex_coord = raw_tex_coord;
  vec4 screen_space = projection * vec4(position, 1.0f);
  gl_Position = screen_space + vec4( 2.0f * offset.xy, 0.0f, 0.0f);
}