#version 450

// IN
layout( location = 0 ) in vec3 position;
layout( location = 1 ) in vec3 normals;
layout( location = 2 ) in vec2 tex_coord;
layout( location = 3 ) in vec3 tangent;
layout( location = 4 ) in vec3 bitangent;

// OUT
layout( location = 0 ) out vec3 out_world_position;
layout( location = 1 ) out vec3 out_normal;
layout( location = 2 ) out vec2 out_tex_coord;
layout( location = 3 ) out vec3 out_tangent;
layout( location = 4 ) out vec3 out_bitangent;

uniform mat4 mvp_matrix;
uniform mat4 normal_matrix;
uniform mat4 model_matrix;

void main() {
	out_tex_coord = tex_coord;
	out_tangent = tangent;
	out_bitangent = bitangent;
  out_world_position = (model_matrix * vec4(position, 1.0)).xyz;
 	out_normal = normalize(normal_matrix * vec4(normals, 0.0f)).xyz;

  gl_Position = mvp_matrix * vec4(position, 1.0);
}